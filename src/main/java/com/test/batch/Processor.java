package com.test.batch;
import java.util.Date;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.test.model.User;

@Component
public class Processor implements ItemProcessor<User, User> {

    public Processor() {
    }

    @Override
    public User process(User user) throws Exception {
        user.setTime(new Date());
        return user;
    }
}