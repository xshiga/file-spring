package com.test.batch;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.test.model.Pessoa;

public class PessoaFieldSetMapper implements FieldSetMapper<Pessoa>{
	
	@Override
    public Pessoa mapFieldSet(FieldSet fieldSet) throws BindException {
    	
    	Pessoa p = new Pessoa();
    	
        p.setNome(fieldSet.readRawString(0));
        p.setIdade(fieldSet.readInt(1));
        p.setData_nascimento(fieldSet.readDate(2, "dd/MM/yyyy"));
        
        return p;
    }

}
