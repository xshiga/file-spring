package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {
}