package com.test.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.model.Pessoa;
import com.test.repository.PessoaRepository;

@Component
public class DBWriterPessoa implements ItemWriter<Pessoa> {

    @Autowired
    private PessoaRepository repository;

    @Override
    public void write(List<? extends Pessoa> pessoas) throws Exception {

        System.out.println("Data Saved for Users: " + pessoas);
        repository.save(pessoas);
    }
}